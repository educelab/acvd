# ACVD

A C++ library to perform fast simplification of 3D surface meshes. This
repository is a non-tracking fork of the
[original ACVD code base](https://github.com/valette/ACVD.git) and has been
reorganized for use as a dependency in EduceLab projects.

##  Dependencies
* VTK 8+
* CMake

## Compilation
```shell
# Get the source code
git clone https://gitlab.com/educelab/acvd.git
cd acvd

# Make a build directory and compile
mkdir build/ && cd build/
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

## Documentation
Visit the Doxygen-generated library documentation [here](https://educelab.gitlab.io/acvd/docs/).

## Licence
This repository contains code that is copyright CNRS, INSA-Lyon, UCBL, INSERM
and is distributed under the CeCILL-B license (BSD-compatible). Per the
CeCILL-B license, additional code contributions by EduceLab are copyright the
contributor. See the [LICENSE](LICENSE.txt) for more details.

## Publications
This code is an implementation of the algorithms described in the following publications:

- S. Valette, J.-M. Chassery and R. Prost, Generic remeshing of 3D triangular meshes with metric-dependent discrete Voronoi Diagrams, _IEEE Transactions on Visualization and Computer Graphics_, Volume 14, no. 2, pages 369-381, 2008.

- Sebastien Valette and Jean-Marc Chassery, Approximated Centroidal Voronoi Diagrams for Uniform Polygonal Mesh Coarsening, _Computer Graphics Forum (Eurographics 2004 proceedings)_, Vol. 23, No. 3, September 2004, pp. 381-389.

- M. Audette, D. Rivière, M. Ewend, A. Enquobahrie, and S. Valette, "Approach-guided controlled resolution brain meshing for FE-based interactive neurosurgery simulation", _Workshop on Mesh Processing in Medical Image Analysis, in conjunction with MICCAI 2011._, Toronto, Canada, pp. 176--186, 09/2011.
