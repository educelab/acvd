# Public headers
file(GLOB _VolumeProcessing_hdrs include/*.h)

add_library(ACVDVolumeProcessing
    src/vtkImageDataCleanLabels.cxx
    src/vtkOOCMetaImageReader.cxx
)
add_library("ACVD::VolumeProcessing" ALIAS ACVDVolumeProcessing)
target_include_directories(ACVDVolumeProcessing
    PUBLIC
      $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
      $<INSTALL_INTERFACE:include/ACVD/VolumeProcessing>
)

target_link_libraries(ACVDVolumeProcessing
    PRIVATE
        VTK::CommonCore
        VTK::CommonExecutionModel
        VTK::IOImage
        VTK::metaio
        VTK::zlib
)

set_target_properties(ACVDVolumeProcessing
  PROPERTIES
    EXPORT_NAME VolumeProcessing
    DEBUG_POSTFIX "-d"
    VERSION "${ACVD_VERSION}"
    SOVERSION "${ACVD_LIB_SOVERSION}"
    PUBLIC_HEADER "${_VolumeProcessing_hdrs}"
)

install(
  TARGETS ACVDVolumeProcessing
  EXPORT ACVDTargets
  PUBLIC_HEADER DESTINATION include/ACVD/VolumeProcessing
)
