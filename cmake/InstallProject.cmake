include(CMakePackageConfigHelpers)
write_basic_package_version_file(
    ACVDConfigVersion.cmake
    COMPATIBILITY SameMajorVersion
)
configure_file(cmake/ACVDConfig.cmake.in ACVDConfig.cmake @ONLY)

# Install exported targets file
install(
    EXPORT ACVDTargets
    FILE ACVDTargets.cmake
    NAMESPACE "ACVD::"
    DESTINATION lib/cmake/ACVD
)

# Install package configs
install(
    FILES
        ${CMAKE_BINARY_DIR}/ACVDConfig.cmake
        ${CMAKE_BINARY_DIR}/ACVDConfigVersion.cmake
    DESTINATION lib/cmake/ACVD
)
