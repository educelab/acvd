option(ACVD_BUILD_VTK "Build VTK" OFF)
if(ACVD_BUILD_VTK)
  message(STATUS "Building VTK from source")
  FetchContent_Declare(
      vtk
      URL https://www.vtk.org/files/release/9.3/VTK-9.3.1.tar.gz
      URL_HASH SHA256=8354ec084ea0d2dc3d23dbe4243823c4bfc270382d0ce8d658939fd50061cab8
      EXCLUDE_FROM_ALL
  )
  set(VTK_USE_SYSTEM_TIFF OFF CACHE INTERNAL "")
  set(VTK_USE_SYSTEM_ZLIB OFF CACHE INTERNAL "")
  FetchContent_MakeAvailable(vtk)
else()
  find_package(VTK 9 REQUIRED NO_MODULE)
endif()
